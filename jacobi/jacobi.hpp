#ifndef JACOBI_HPP
#define	JACOBI_HPP

#include <iostream>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <cmath>


using namespace boost::numeric::ublas;


/**
 * \brief Szablon funkcji wykonuj¹cej jedn¹ iteracjê algorytmu jacobiego
 *
 * \param Matrix A - Macierz kwadratowa z danymi wejœciowymi
 * \param Vector B - Wektor z danymi wejœciowymi
 * \param Vector x - Wektor z wynikami poprzedniej iteracji
 *
 * \return Matrix res - Macierz z wynikami aktualnej iteracji
 */
template<class Matrix, class Vector>
Vector
jacobi_algorithm_iteration_omp(const Matrix& A, const Vector& B, const Vector& x){

    //Wektor na rezulaty iteracji
    Vector res(x.size());
    size_t n,m, nsize;

    typedef typename Vector::value_type T;

    nsize = x.size();

    //Ka¿dy x wektora wynikowego jest wyliczany oddzielnie
    //#pragma omp parallel for
    #pragma omp parallel for private(n,m) shared(nsize,res)
    for( n = 0; n < nsize ; n ++  ){

        //Do obliczenia x(n) wymagane jest obliczenie sumy
        //wszystkich elementów odpowiadaj¹cego jej wiersza n
        //za wyj¹tkiem elementu A(n,n)
        T sum = (T)0;

        for( m = 0; m < nsize ; m ++){
            if(n != m) sum += A(n,m)*x(m);
        }

        //Oblczenie wartoœci x(n) wektora wynikowego.
        res(n) = (1/A(n,n))*(B(n) - sum);

    }
   // std::cout << res(2);
    return res;
}

/**
 * \brief Metoda wyliczająca poprawnośc rozwiazania w wketorze x
 *
 * \param Matrix A - Macierz kwadratowa z danymi wejœciowymi
 * \param Vector B - Wektor z danymi wejœciowymi
 * \param Vector x - Wektor z wynikami aktualnej iteracji

 * \return Średnia róznica miością oczekiwaną a wyliczoną
 *
 */

template <class Matrix, class Vector, class T>
T
jacobi_algorithm_eps_omp( Matrix& A, Vector& B, Vector& x)
{
    Vector v(x.size());
    size_t i,j;
    size_t nsize = A.size1();

    #pragma omp parallel for private(i,j) shared(nsize,v)
    for ( i = 0; i < nsize; ++ i){
        T sum = T(0);

        for ( j = 0; j < nsize; ++ j){
            sum += A(i,j)*x(j);
        }

        v(i)= B(i)-sum;
        //std::cout << B(i) << " " << sum << " suma: "<< B(i) - sum << std::endl;
    }

    T sum = (T)0;

    #pragma omp parallel for private(j) shared(v) reduction(+ : sum)
    for( j = 0; j < v.size(); ++ j) {
        sum += abs(v(j));
    }

    return sum/v.size();
}


/**
 * \brief Szablon funkcji do generowania wypelnionej macierzy kwadratowej
 *
 * \param size_t size - Liczba kolumn i wierszy macierzy wynikowej
 * \param size_t d - Procentowa gêstoœæ macierzy
 * \param boost::mt19937 gen - Obiekt do generowania liczb pseudolosowych
 *
 * \return Matrix macierz wynikowa
 *
 * Generowana macierz zawsze jest diagonalnie zdominowana, symetryczna oraz
 *dodatnio zoientowana
 *
 */
template <class Matrix>
Matrix
matrix_generator(size_t nsize, size_t d, boost::mt19937 gen) {

    typedef typename Matrix::value_type T;

    Matrix res(nsize, nsize);
    boost::random::uniform_int_distribution<> r(1,100);
    boost::random::uniform_int_distribution<> v(1,2);
    size_t n,m;


    for( n = 0; n < nsize; n ++ ){
        for( m = 0; m < n; m++){

            if( n != m ) {

                if( (size_t)r(gen) < d){

                    res(n,m) = (T)v(gen);
                    res(m,n) = res(n,m);

                }
            }
        }
    }

    for( n = 0; n < nsize; n ++ ){

        for( m = 0; m < nsize; m ++)
            if(n != m) res(n,n) += res(n,m);
            res(n,n) += 1;
    }


    return res;

}



/**
 * \brief Szablon funkcji do generowania wype³nionego wektora liczb
 *
 * \param size_t size - Rozmiar wektora wynikowego
 * \param empty - Wspó³czynniki determinujacy zawartoœci wektora wynikowego
 * \param boost::mt19937 gen - Obiekt do generowania liczb pseudolosowych
 *
 * \return  Vector - wektor wynikowy
 *
 * Wygenerowany zostanie wektor z liczbami pseudolosowymi, albo z samymi zerami
 * w zaleznoœci od parametru "empty".
 */
template <class Vector>
Vector
vector_generator(size_t size, bool empty, boost::mt19937 gen) {

    typedef typename Vector::value_type T;

    Vector res(size);
    boost::random::uniform_int_distribution<> r(1,50);

    for(size_t n = 0; n < size ; n++){
        (!empty) ? res(n) = r(gen) : res(n) = 0;
    }

    return res;
}

#endif

