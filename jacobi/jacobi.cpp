#include "jacobi.hpp"
#include <boost/progress.hpp>
#include <boost/lexical_cast.hpp>
//#include "omp.h"


int main(int argc, char *argv[])
{

    size_t NW,LOOPS;

    if(argc == 3){

        NW = boost::lexical_cast<int>(argv[1]);
        LOOPS = boost::lexical_cast<int>(argv[2]);

    } else if(argc == 2) {

        NW = NW = boost::lexical_cast<int>(argv[1]);
        LOOPS = 10;

    } else {

        NW = 2000;
        LOOPS = 10;
    }

    boost::random::mt19937 rng;
    mapped_matrix<double> A = matrix_generator< mapped_matrix<double> > (NW,5,rng);
    vector<double> B = vector_generator< vector<double> > (NW,false,rng);
    vector<double> x = vector_generator< vector<double> > (NW,true,rng);


    vector<double> res = x;
    double eps;

    boost::progress_timer t;

    for(size_t i = 0 ; i < LOOPS ;  i++ ) {

    res = jacobi_algorithm_iteration_omp< mapped_matrix<double>,vector<double> >(A,B,x);


    eps = jacobi_algorithm_eps_omp< mapped_matrix<double>,vector<double>, double >(A,B,x);
   // std::cout << "====== Koniec "<< i << " =========" << "blad = " << eps  << std::endl;


    x = res;
    if(eps < 0.00001 && eps > -0.00001) break;

    }

return 0;
}


