#define __CL_ENABLE_EXCEPTIONS
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/progress.hpp>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <iostream>
#include <iterator>
#include <CL/cl.hpp>
//#include <CL/opencl.h>
#include "gen.h"


using namespace std;

int main (int argc, char *argv[]) {

	size_t nsize = 100;
	int maxiter = 5000;

	if(argc == 2) {

        nsize = boost::lexical_cast<int>(argv[1]);
    }


    vector<cl::Platform> platforms;
    vector<cl::Device> devices;
    vector<cl::Kernel> kernels;




	// generowanie danych
    boost::mt19937 gen;
	// macierz jest generowana w formie jednowymiarowej tablicy
	// [n][m] = [n*rozmiar+m]
	float *A = matrix_generator(nsize,5,gen); 
	float *B = vector_generator(nsize,false,gen); 
	float *x = vector_generator(nsize,true,gen);
	float *xn = vector_generator(nsize,true,gen); //macierz na wynik kolejnej iteracji
	float *ep = vector_generator(nsize,true,gen);

	//for(size_t n = 0; n < nsize; n ++ )
 //       std::cout << B[n] << std::endl;
    


 //   float *res = matrix_generator(nsize,50,gen);
 //   for(size_t n = 0; n < nsize; n ++ ){
 //       for(size_t m = 0; m < nsize; m ++)
 //           std::cout << res[n*nsize + m] << " ";
 //       std::cout << std::endl;
 //   }

    
    try {
    
        // wyszukujemy urzadzenia i dodajmey gpu do listy
        cl::Platform::get(&platforms);
        platforms[0].getDevices(CL_DEVICE_TYPE_CPU, &devices);

        // tworzenie contekstu
        cl::Context context(devices);

        // kolejska zadan
		// jest asynchroniczna, wi�c dodanie zadania do niej
		// nie jest blokujace (chyba ze chcemy)
        cl::CommandQueue queue(context, devices[0]);

        // otwieramy i wczytujemy plik z kernelami
        ifstream cl_file("jacobi.cl");
        string cl_string(istreambuf_iterator<char>(cl_file), (istreambuf_iterator<char>()));
        cl::Program::Sources source(1, make_pair(cl_string.c_str(), 
            cl_string.length() + 1));

        // a nast�pnie tworzymy z nich program
        cl::Program program(context, source);

        // i go kompilujemy
        program.build(devices);
		
        // tworzymy dwa kernele
		// po jednym dla kazdej funkcji
        cl::Kernel iteration(program, "iteration");
		cl::Kernel eps(program, "eps");

		// tworzymy bufory w pamieci kart graficznej
		cl::Buffer b_A(context,CL_MEM_READ_ONLY,nsize*nsize*sizeof(float));
		cl::Buffer b_B(context,CL_MEM_READ_ONLY,nsize*sizeof(float));
		cl::Buffer b_x(context,CL_MEM_READ_ONLY,nsize*sizeof(float));
		cl::Buffer b_xn(context,CL_MEM_READ_WRITE,nsize*sizeof(float));
		cl::Buffer b_ep(context,CL_MEM_WRITE_ONLY,nsize*sizeof(float));

		//wczytujemy dane do bufor�w, ale tylko niezbedne
		queue.enqueueWriteBuffer(b_A, CL_TRUE, 0, nsize * nsize * sizeof(float), A);
		queue.enqueueWriteBuffer(b_B, CL_TRUE, 0, nsize * sizeof(float), B);
		queue.enqueueWriteBuffer(b_x, CL_TRUE, 0, nsize * sizeof(float), x);

		//ustawiamy argumenty dla funkcji
		// przeprowadzajcej iteracje algorytmu
		iteration.setArg(0,b_A);
		iteration.setArg(1,b_B);
		iteration.setArg(2,b_x);
		iteration.setArg(3,b_xn);
		iteration.setArg(4,sizeof(size_t), &nsize);

		//ustawiamy argumenty dla
		//funkcji sprawdzajacej wynik
		eps.setArg(0,b_x);
		eps.setArg(1,b_xn);
		eps.setArg(2,b_ep);

		//ustawiamy wielkosc grupy roboczej
		cl::NDRange global(nsize);
        cl::NDRange local(1);

		int iter = 0;
		boost::progress_timer t;

		//petla wykonujaca kolejne iteracje
		while(iter < maxiter)
		{

			//dodajemy do kolejki zadanie wykonania iteracji algorytmu
			//program z kernela jest wykonywany tyle razy ile jest grup lokalnych
			//czyli doklanie tyle ile wynosi rozmiar macierzy
			queue.enqueueNDRangeKernel(iteration, cl::NullRange, global, local);
			//a nastepnie odczytujemy wyniki, CL_TRUE - program blokuje sie do momentu odczytania 
			//wszystkiego
			queue.enqueueReadBuffer(b_xn, CL_TRUE, 0, nsize * sizeof(float), xn);

			//for(size_t n = 0; n < nsize; n ++ )
	  //      std::cout << xn[n] << std::endl;

			//std::cout << "==================" << std::endl;

			//tak jak wyzej ale dla drugiej funkcji/kernela
			queue.enqueueNDRangeKernel(eps, cl::NullRange, global, local);
			queue.enqueueReadBuffer(b_ep, CL_TRUE, 0, nsize * sizeof(float), ep);

			//for(size_t n = 0; n < nsize; n ++ )
	  //      std::cout << ep[n] << std::endl;

			//obliczanie bledu
			float epsilon = 0;

			for(size_t n = 0; n < nsize; n ++ )
			{
				epsilon += ep[n]; 
			}

			epsilon = epsilon/nsize;

			std::cout << "========= Iteracja: "<< ++iter << " eps: " << epsilon << " =========" << std::endl;

			if(epsilon < 0.001 && epsilon > -0.001) break;

			//aktualizacja x na potrzeby kolejnej iteracji
			queue.enqueueWriteBuffer(b_x, CL_TRUE, 0, nsize * sizeof(float), xn);

		}

        
    } catch (cl::Error e) {
        cout << endl << e.what() << " : " << e.err() << endl;
    }

	//wyrzucamy zb�dne informacje
	delete [] A;
	delete [] B;
	delete [] x;
	delete [] xn;
	delete [] ep;
    
    return 0;
    
}
