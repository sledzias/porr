__kernel void iteration(__global const float *A, 
						__global const float *B,
						__global const float *x,
                        __global float *xn,
						const int nsize)
{
    float sum = 0;
	int m;
	int n = get_global_id(0);
	
	for( m = 0; m < nsize ; ++m ){
		if(n != m) sum += A[n*nsize+m]*x[m];
    }


    xn[n] = (1/A[n*nsize+n])*(B[n] - sum);

}

__kernel void eps(__global const float *x,
                        __global float *xn,
						__global float *res)
{
	float df;
	int n = get_global_id(0);
	df = x[n] - xn[n];
	if(df < 0) df = df * -1;
	res[n] = df;
}