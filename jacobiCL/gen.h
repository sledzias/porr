#ifndef GEN_H
#define GEN_H

float *matrix_generator(size_t nsize, size_t d,boost::mt19937 gen);
float *vector_generator(size_t nsize, bool is_empty,boost::mt19937 gen);


#endif // GEN_H
