#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>
#include "gen.h"

float *matrix_generator(size_t nsize, size_t d,boost::mt19937 gen)
{

    float *res = new float[nsize*nsize];

    boost::random::uniform_int_distribution<> r(1,100);
    boost::random::uniform_int_distribution<> v(1,2);
    size_t n,m;


    for( n = 0; n < nsize; n ++ ){
        for( m = 0; m < n; m++){

            if( n != m ) {

                if( (size_t)r(gen) < d){

                    res[n*nsize + m] = (float)v(gen);
                    res[m*nsize + n] = res[n*nsize + m];

                } else
                {
                    res[n*nsize + m] = (float)0;
                    res[m*nsize + n] = res[n*nsize + m];
                }
            }
        }
    }


    for( n = 0; n < nsize; n ++ ){

		res[n*nsize + n] = 0;

        for( m = 0; m < nsize; m ++)
            if(n != m) res[n*nsize + n] += res[n*nsize + m];

            res[n*nsize + n] += 1;
    }


    return res;

}


float* vector_generator(size_t nsize, bool is_empty,boost::mt19937 gen)
{
	float *res = new float[nsize];
    boost::random::uniform_int_distribution<> r(1,50);

    for(size_t n = 0; n < nsize ; n++){
        (!is_empty) ? res[n] = r(gen) : res[n] = 0;
    }

    return res;
}