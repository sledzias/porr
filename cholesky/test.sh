#!/bin/bash
if [ 'x'$2 != 'x' ]
then	
	export OMP_NUM_THREADS=$2
else
	export OMP_NUM_THREADS=2
fi
echo "Liczba watkow: $OMP_NUM_THREADS"
./cholesky $1

./cholesky_vect $1
 
./cholesky_omp $1