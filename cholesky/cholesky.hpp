#ifndef CHOLESKY_HPP
#define CHOLESKY_HPP

#ifdef _OPENMP
   #include <omp.h>
#else
   #define omp_get_thread_num() 0
   #define omp_get_num_threads() 0
   #define omp_set_num_threads(int) 0
#endif

#include <cmath>
#include <time.h>
#include <boost/random/uniform_real_distribution.hpp>

#include <iostream>
#include <boost/numeric/ublas/matrix_sparse.hpp>
#include <boost/numeric/ublas/io.hpp>

#include <boost/random/mersenne_twister.hpp>
#include <boost/random/discrete_distribution.hpp>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>

#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>

#include <boost/numeric/ublas/vector_expression.hpp>
#include <boost/numeric/ublas/matrix_expression.hpp>

#include <boost/numeric/ublas/triangular.hpp>



using namespace std;
using namespace boost::numeric::ublas;

 

template <class Matrix>
void 
print_matrix(Matrix& A)
{
  if (true) return;
  if (A.size1() > 5) return;
typedef typename Matrix::value_type& T;
    for (unsigned int i=0; i<A.size1(); ++i){
        for (unsigned int j=0; j<A.size2(); ++j){
            cout << A(i,j)<<  '\t';
            }       
        cout <<    endl ;

    }
    return;

}

template < class MATRIX>
size_t cholesky_decompose2(MATRIX& A, MATRIX& L)
{
  // omp_set_num_threads(4);
  typedef typename MATRIX::value_type T;
 
  const size_t n = A.size1();

  for (size_t k=0; k < n-1; ++k)
  {
      L(k,k) = sqrt(A(k,k));

      for (size_t i=k+1; i<n; ++i)
      {
        L(i,k)=A(i,k)/L(k,k);
      }
         
      #pragma omp parallel for  schedule(static)
      for (size_t j=k+1; j<n; ++j)
      {
        
        for (size_t i = j; i<n;++i)
        {
          A(i,j) = A(i,j) - L(i,k)*L(j,k);
        }
      }

  }
  L(n-1,n-1) = sqrt(A(n-1,n-1));

  
  
  return 0;      
}





/**
 * @brief Szablon funkcji do generowania wype³nionej macierzy kwadratowej
 *
 * @param size_t size - Liczba kolumn i wierszy macierzy wynikowej
 * @param size_t d - Procentowa gêstoœæ macierzy
 * @param boost::mt19937 gen - Obiekt do generowania liczb pseudolosowych
 *
 * @return Matrix macierz wynikowa
 *
 * Generowana macierz zawsze jest diagonalnie zdominowana.
 *
 */
template <class Matrix>
Matrix
matrix_generator(size_t nsize, size_t d, boost::mt19937 gen) {

    typedef typename Matrix::value_type T;

    Matrix res(nsize, nsize);
    boost::random::uniform_int_distribution<> r(1,100);
    boost::random::uniform_int_distribution<> v(1,2);
    size_t n,m;


    for( n = 0; n < nsize; n ++ ){
        for( m = 0; m < n; m++){

            if( n != m ) {

                if( (size_t)r(gen) < d){

                    res(n,m) = (T)v(gen);
                    res(m,n) = res(n,m);

                }
            }
        }
    }

    for( n = 0; n < nsize; n ++ ){

        for( m = 0; m < nsize; m ++)
            if(n != m) res(n,n) += res(n,m);
            res(n,n) += 1;
    }


    return res;

}



#endif