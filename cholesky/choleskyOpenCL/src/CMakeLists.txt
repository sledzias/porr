add_executable( cholesky cholesky.cpp )
target_link_libraries( cholesky ${OPENCL_LIBRARIES} )

configure_file(cholesky5.cl ${CMAKE_CURRENT_BINARY_DIR}/cholesky5.cl COPYONLY)
