
// obliczenie x dla b=Ax dla podanego A i b

kernel void cholesky5(
    global float const * As,
    global float const * bs,
    global float       * xs
) {

    //ustalenie ktora macierz ma byc liczona na podstawie id globalnego
    int const imatrix   = get_global_id(0);
    int const nmatrices = get_global_size(0);

    //pobieranie elementow macierzy A
    float r0c0 = As[mad24(  0, nmatrices, imatrix)];
    float r0c1 = As[mad24(  1, nmatrices, imatrix)];
    float r0c2 = As[mad24(  2, nmatrices, imatrix)];
    float r0c3 = As[mad24(  3, nmatrices, imatrix)];
    float r0c4 = As[mad24(  4, nmatrices, imatrix)];
    float r1c0 = As[mad24(  5, nmatrices, imatrix)];
    float r1c1 = As[mad24(  6, nmatrices, imatrix)];
    float r1c2 = As[mad24(  7, nmatrices, imatrix)];
    float r1c3 = As[mad24(  8, nmatrices, imatrix)];
    float r1c4 = As[mad24(  9, nmatrices, imatrix)];
    float r2c0 = As[mad24( 10, nmatrices, imatrix)];
    float r2c1 = As[mad24( 11, nmatrices, imatrix)];
    float r2c2 = As[mad24( 12, nmatrices, imatrix)];
    float r2c3 = As[mad24( 13, nmatrices, imatrix)];
    float r2c4 = As[mad24( 14, nmatrices, imatrix)];
    float r3c0 = As[mad24( 15, nmatrices, imatrix)];
    float r3c1 = As[mad24( 16, nmatrices, imatrix)];
    float r3c2 = As[mad24( 17, nmatrices, imatrix)];
    float r3c3 = As[mad24( 18, nmatrices, imatrix)];
    float r3c4 = As[mad24( 19, nmatrices, imatrix)];
    float r4c0 = As[mad24( 20, nmatrices, imatrix)];
    float r4c1 = As[mad24( 21, nmatrices, imatrix)];
    float r4c2 = As[mad24( 22, nmatrices, imatrix)];
    float r4c3 = As[mad24( 23, nmatrices, imatrix)];
    float r4c4 = As[mad24( 24, nmatrices, imatrix)];

   // pobieranie elementow wektora b
    float v0   = bs[mad24(  0, nmatrices, imatrix)];
    float v1   = bs[mad24(  1, nmatrices, imatrix)];
    float v2   = bs[mad24(  2, nmatrices, imatrix)];
    float v3   = bs[mad24(  3, nmatrices, imatrix)];
    float v4   = bs[mad24(  4, nmatrices, imatrix)];

    /* Kolumna 0 */ {
        /* Wiersz 0 */ {
            float val = r0c0;
            r0c0      = val;
        }
        /* Wiersz 1 */ {
            float val = r1c0;
            r0c1      = val;
            r1c0      = (val / r0c0);
        }
        /* Wiersz 2 */ {
            float val = r2c0;
            r0c2      = val;
            r2c0      = (val / r0c0);
        }
        /* Wiersz 3 */ {
            float val = r3c0;
            r0c3      = val;
            r3c0      = (val / r0c0);
        }
        /* Wiersz 4 */ {
            float val = r4c0;
            r0c4      = val;
            r4c0      = (val / r0c0);
        }
    }

    /* Kolumna 1 */ {
        /* Wiersz 1 */ {
            float val = r1c1;
            val      -= (r0c1 * r1c0);
            r1c1      = val;
        }
        /* Wiersz 2 */ {
            float val = r2c1;
            val      -= (r0c1 * r2c0);
            r1c2      = val;
            r2c1      = (val / r1c1);
        }
        /* Wiersz 3 */ {
            float val = r3c1;
            val      -= (r0c1 * r3c0);
            r1c3      = val;
            r3c1      = (val / r1c1);
        }
        /* Wiersz 4 */ {
            float val = r4c1;
            val      -= (r0c1 * r4c0);
            r1c4      = val;
            r4c1      = (val / r1c1);
        }
    }

    /* Kolumna 2 */ {
        /* Wiersz 2 */ {
            float val = r2c2;
            val      -= (r0c2 * r2c0);
            val      -= (r1c2 * r2c1);
            r2c2      = val;
        }
        /* Wiersz 3 */ {
            float val = r3c2;
            val      -= (r0c2 * r3c0);
            val      -= (r1c2 * r3c1);
            r2c3      = val;
            r3c2      = (val / r2c2);
        }
        /* Wiersz 4 */ {
            float val = r4c2;
            val      -= (r0c2 * r4c0);
            val      -= (r1c2 * r4c1);
            r2c4      = val;
            r4c2      = (val / r2c2);
        }
    }

    /* Kolumna 3 */ {
        /* Wiersz 3 */ {
            float val = r3c3;
            val      -= (r0c3 * r3c0);
            val      -= (r1c3 * r3c1);
            val      -= (r2c3 * r3c2);
            r3c3      = val;
        }
        /* Wiersz 4 */ {
            float val = r4c3;
            val      -= (r0c3 * r4c0);
            val      -= (r1c3 * r4c1);
            val      -= (r2c3 * r4c2);
            r3c4      = val;
            r4c3      = (val / r3c3);
        }
    }

    /* Kolumna 4 */ {
        /* Wiersz 4 */ {
            float val = r4c4;
            val      -= (r0c4 * r4c0);
            val      -= (r1c4 * r4c1);
            val      -= (r2c4 * r4c2);
            val      -= (r3c4 * r4c3);
            r4c4      = val;
        }
    }


    // podstawianie wstecz na macierzy L
    v1 -= (r1c0 * v0);
    v2 -= (r2c0 * v0);
    v2 -= (r2c1 * v1);
    v3 -= (r3c0 * v0);
    v3 -= (r3c1 * v1);
    v3 -= (r3c2 * v2);
    v4 -= (r4c0 * v0);
    v4 -= (r4c1 * v1);
    v4 -= (r4c2 * v2);
    v4 -= (r4c3 * v3);


    // podstawianie wstecz na diagonali
    v0 /= r0c0;
    v1 /= r1c1;
    v2 /= r2c2;
    v3 /= r3c3;
    v4 /= r4c4;

    // podstawianie wstecz na macierzy L^t
    v3 -= (r4c3 * v4);
    v2 -= (r3c2 * v3);
    v2 -= (r4c2 * v4);
    v1 -= (r2c1 * v2);
    v1 -= (r3c1 * v3);
    v1 -= (r4c1 * v4);
    v0 -= (r1c0 * v1);
    v0 -= (r2c0 * v2);
    v0 -= (r3c0 * v3);
    v0 -= (r4c0 * v4);

    // Zapusywanie wynikow
    xs[mad24(  0, nmatrices, imatrix)] = v0;
    xs[mad24(  1, nmatrices, imatrix)] = v1;
    xs[mad24(  2, nmatrices, imatrix)] = v2;
    xs[mad24(  3, nmatrices, imatrix)] = v3;
    xs[mad24(  4, nmatrices, imatrix)] = v4;
}
