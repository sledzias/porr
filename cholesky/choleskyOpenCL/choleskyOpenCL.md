# Projekt PORR cz.2 
## Implementacja faktoryzacji metodą Choleskiego z użyciem OpenCL

###Wymagania
Projekt działa w środowisku Linux/Unix. Wymagane są biblioteki TooN, Boost oraz zainstalowany OpenCL.

### Instalacja biblioteki TooN
W celu zainstalowania należy przejść do katalogu ```cholesky/choleskyOpenCL/TooN``` , następnie wykonać polecenie:

```
./configure && make install
```

### Kompilacja i uruchomienie projektu
W celu skompilowania i uruchomienia projektu należy przejść do katalogu  ```cholesky/choleskyOpenCL/build``` i wykonać polecenie:

```
cd ../ ; rm -r build/* ; cd ./build ; cmake ../ ; make && cd ./src  && ./cholesky && cd ../
```

Spodziewany wynik wykonania programu jest następujący:

```
Wygenerowanie 1000 losowych macierzy 5x5
obliczanie biblioteka TooN Cholesky
    6165 us -  czas przetwarzania biblioteka TooN Cholesky
total[0] = -6.77487
total[1] = -13.8555
total[2] = 13.4963
total[3] = 9.79412
total[4] = 0.322039
obliczanie przy pomocy OpenCL
Apple (Apple, OpenCL 1.2 (Sep 20 2012 17:42:28))
  Intel(R) Core(TM) i5-3427U CPU @ 1.80GHz
    liczba jednostek obliczeniowych:         4
    Pamiec globalna:      8192 MiB
      40 us - czas przetwarzania wykorzystujac OpenCL
total[0] = -6.77487
total[1] = -13.8555
total[2] = 13.4963
total[3] = 9.79412
total[4] = 0.32204
```

### Opis działania programu
Program generuje 1000 losowych macierzy 5x5 (A) oraz 1000 wektorów (b), a następnie rozwiązuje układ ``` b=Ax ``` dla każdej macierzy. Cząstkowe wyniki x są sumowane do jednego wektora i prezentowane na wyjściu programu (pomaga to w weryfikacji poprawności obliczeń). Faktoryzacja jest obliczana na dwa sposoby, najpierw przy użyciu biblioteki TooN, a następnie przy użyciu OpenCL. Obliczanie przy pomocy bibioteki TooN jest potrzebne, aby zobaczyć różnicę w wykorzystaniu możliwości OpenCL oraz w celu weryfikacji poprawności obliczeń.