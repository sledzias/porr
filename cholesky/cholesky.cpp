#include "cholesky.hpp"
#include <boost/timer.hpp>
#include <boost/lexical_cast.hpp>


bool 
test_cholesky(mapped_matrix<double> &A, mapped_matrix<double> &L)
{
 		mapped_matrix<double> L_t = trans(L);
  		mapped_matrix<double> res = prod(L,L_t);
  
	bool ret = true;
	for (unsigned int i=0; i<A.size1(); ++i){
		for (unsigned int j=0; j<A.size2(); ++j){
		
			if (A(i,j)-res(i,j) > 0.001 || A(i,j)-res(i,j)< -0.001  ){
				ret = false;
				break;
			}

			if (!ret) break;
		} 			
	} 	
	return ret;
  
}

int main(int argc, char const *argv[])
{
	size_t NW;

    if(argc == 2){

        NW = boost::lexical_cast<int>(argv[1]);
       

    } else if(argc == 1) {

       NW = 2000;
       

    } else {
    	cout << "użycie: " << argv[0] << " [rozmiar macierzy = 2000]" << endl;
        return 1;
    }

    
	boost::random::mt19937 rng;
    mapped_matrix<double> A = matrix_generator< mapped_matrix<double> > (NW,30,rng);
	mapped_matrix<double> B = A;
	
	mapped_matrix<double> L(NW,NW);
	boost::timer t;
	cholesky_decompose2< mapped_matrix<double> >(B,L);
	double dec_time = t.elapsed();
	print_matrix< mapped_matrix<double> >(L);
	cout << argv[0];
	if (test_cholesky(A,L)){
		cout << "\tOK";
	}
	else{
		cout << "\tBLAD";

	}

	cout << "\t" << dec_time << endl;
	return 0;
}

